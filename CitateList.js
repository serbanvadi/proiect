import React from 'react';

export class CitateList extends React.Component {
    // constructor(props){
    //     super(props);
        
    // }
    
    render(){
        let items = this.props.source.map((citat, index) =>{
            return <div key={index}>Id: {citat.id_citat} - Citat: {citat.citat} </div>
                
        })
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div>
                {items}
                </div>
            </div>
            );
    }
}