import React from 'react';

export class BookmarkList extends React.Component {
    // constructor(props){
    //     super(props);
        
    // }
    
    render(){
        let items = this.props.source.map((bookmark, index) =>{
            return <div key={index}>Id: {bookmark.idBookmark} - Link: {bookmark.linkFavorit} </div>
                
        })
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div>
                {items}
                </div>
            </div>
            );
    }
}