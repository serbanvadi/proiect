import React from 'react';
import axios from 'axios';

export class AddUtilizator extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.nume = ""
        this.state.prenume = ""
        this.state.tara = "";
        this.state.utilizator = "";
        this.state.parola = "";
    }
    
    
    handleChangeNume = (event) => {
        this.setState({
            nume: event.target.value
        })
    }
    
    handleChangePrenume = (event) => {
        this.setState({
            prenume: event.target.value
        })
    }
    
    handleChangeTara = (event) => {
        this.setState({
            tara: event.target.value
        })
    }
      handleChangeUtilizator = (event) => {
        this.setState({
            utilizator: event.target.value
        })
    }
      handleChangeParola = (event) => {
        this.setState({
            parola: event.target.value
        })
    }
    
    
    handleAddClick = () => {
        let utilizator = {
            nume: this.state.nume,
            prenume: this.state.prenume,
            tara: this.state.tara,
            utilizator:this.state.utilizator,
            parola:this.state.parola
        }
        
        axios.post('https://serban-serbanadi.c9users.io/inregistrare', utilizator).then((res) => {
            if(res.status === 200){
                this.props.utilizatorAdded(utilizator)
            }
        }).catch((err) =>{
            console.log(err)
        })
        
        
    }
        
    render(){
        return(
            <div>
                <h1>Intregistreaza utilizator</h1>
                <input type="text" placeholder="Nume" 
                    onChange={this.handleChangeNume}
                    value={this.state.nume} />
                <input type="text" placeholder="Prenume" 
                    onChange={this.handleChangePrenume}
                    value={this.state.prenume} />
                 <input type="text" placeholder="Tara" 
                    onChange={this.handleChangeTara}
                    value={this.state.tara} />
                     <input type="text" placeholder="Utilizator" 
                    onChange={this.handleChangeUtilizator}
                    value={this.state.utilizator} />
                     <input type="text" placeholder="Parola" 
                    onChange={this.handleChangeParola}
                    value={this.state.parola} />
                <button onClick={this.handleAddClick}>Inregistreaza-te</button>
            </div>
            );
    }
}