import React from 'react';

export class UtilizatorList extends React.Component {
    // constructor(props){
    //     super(props);
        
    // }
    
    render(){
        let items = this.props.source.map((utilizator, index) =>{
            return <div key={index}>Nume: {utilizator.nume} - Prenume: {utilizator.prenume} - Tara: {utilizator.tara} - Username: {utilizator.utilizator}</div>
                
        })
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div>
                {items}
                </div>
            </div>
            );
    }
}