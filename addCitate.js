import React from 'react';
import axios from 'axios';
let x=0;

export class AddCitate extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.id_citat = ""
        this.state.citat = ""
    }
    
    
    // handleChangeIdCitat = (event) => {
    //     this.setState({
    //         id_citat: event.target.value
    //     })
    // }
    
    handleChangeCitat = (event) => {
        this.setState({
            citat: event.target.value
        })
    }
    
   
    handleAdaugaCitat = () => {
        let citat = {
            id_citat: x++,
            citat: this.state.citat
        }
        axios.post('https://serban-serbanadi.c9users.io/citateFavorite', citat).then((res) => {
            if(res.status === 200){
                this.props.utilizatorAdded(citat)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
        
    render(){
        return(
             <div>
                <h1>Citate favorite</h1>
               
                <input type="text" placeholder="Citat  favorit" 
                    onChange={this.handleChangeCitat}
                    value={this.state.citat} />
                <button onClick={this.handleAdaugaCitat}>Adauga</button>
            </div>
            );
    }
}