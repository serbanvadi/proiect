import React, { Component } from 'react';
import './App.css';

import {AddUtilizator} from './componente/AddUtilizator';
import {UtilizatorList} from './componente/UtilizatorList';
import {AddIstoric} from './componente/addIstoric';
import {IstoricList} from './componente/IstoricList';
import {AddBookmark} from './componente/addBookmark';
import {BookmarkList} from './componente/BookmarkList';
import {AddCitate} from './componente/addCitate';
import {CitateList} from './componente/CitateList';


class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.utilizators = [];
    this.state.bookmarks=[];
    this.state.citate=[];
    this.state.istoric=[];
  }
  
  onutilizatorAdded = (utilizator) => {
    let utilizators = this.state.utilizators;
    utilizators.push(utilizator);
    this.setState({
      utilizators: utilizators
    });
  }
  
  onBookmarkAdded = (bookmark) => {
    let bookmarks = this.state.bookmarks;
    bookmarks.push(bookmark);
    this.setState({
      bookmarks: bookmarks
    });
  }
  
  onCitatAdded = (citat) => {
    let citate = this.state.citate;
    citate.push(citat);
    this.setState({
      citate: citate
    });
  }
  
  onIstoricAdded = (ist) => {
    let istoric = this.state.istoric;
    istoric.push(ist);
    this.setState({
      istoric: istoric
    });
  }
  
  componentWillMount(){
    const url = 'https://serban-serbanadi.c9users.io/get-all-users'
    fetch(url).then((res) => {
      return res.json();
    }).then((utilizators) =>{
      this.setState({
        utilizators: utilizators
      })
    })
  
    
  const url2 = 'https://serban-serbanadi.c9users.io/get-all-history'
    fetch(url2).then((res) => {
      return res.json();
    }).then((istoric) =>{
      this.setState({
        istoric: istoric
      })
    })
    
    const url3 = 'https://serban-serbanadi.c9users.io/get-all-bookmarks'
    fetch(url3).then((res) => {
      return res.json();
    }).then((bookmarks) =>{
      this.setState({
        bookmarks: bookmarks
      })
    })
    
    const url4 = 'https://serban-serbanadi.c9users.io/get-all-favourite-quotes'
    fetch(url4).then((res) => {
      return res.json();
    }).then((citate) =>{
      this.setState({
        citate: citate
      })
    })
     
  }
  render() {
    return (
      <React.Fragment>
        <AddUtilizator utilizatorAdded={this.onutilizatorAdded}/>
        <UtilizatorList title="Utilizatori inregistrati" source={this.state.utilizators} />
         <AddBookmark bookmarkAdded={this.onBookmarkAdded}/>
        <BookmarkList title="Bookmark salvat" source={this.state.bookmarks} />
           
         <AddCitate citateAdded={this.onCitatAdded}/>
         <CitateList title="Citate favorite" source={this.state.citate} />
         
         <AddIstoric istoricAdded={this.onIstoricAdded}/>
         <IstoricList title="Istoric List" source={this.state.istoric}/>
      </React.Fragment>
 
    );
  }
}

export default App;
