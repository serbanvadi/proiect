import React from 'react';
import axios from 'axios';
let x=0;

export class AddBookmark extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.idBookmark = ""
        this.state.linkFavorit = ""
    }
    
    // handleChangeIdBookmark = (event) => {
    //     this.setState({
    //         idBookmark: event.target.value
    //     })
    // }
    
    handleChangeLinkFavorit = (event) => {
        this.setState({
            linkFavorit: event.target.value
        })
    }
    
    
    handleAdaugaBookmarkClick = () => {
        let bookmark1 = {
            idBookmark: x++,
            linkFavorit: this.state.linkFavorit
          
        }
        axios.post('https://serban-serbanadi.c9users.io/bookmarks', bookmark1).then((res) => {
            if(res.status === 200){
                this.props.bookmarkAdded(bookmark1)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
        
    render(){
        return(
             <div>
                <h1>Bookmarks</h1>
                
                <input type="text" placeholder="Link  favorit" 
                    onChange={this.handleChangeLinkFavorit}
                    value={this.state.linkFavorit} />
                <button onClick={this.handleAdaugaBookmarkClick}>Salveaza</button>
            </div>
            );
    }
}