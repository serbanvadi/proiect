import React from 'react';
import axios from 'axios';
let x=0;

export class AddIstoric extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.idIstoric = ""
        this.state.textCautat = ""
      //  this.state.linkAccesat = ""
    }
    
    
    handleChangeIdIstoric = (event) => {
        this.setState({
            idIstoric: event.target.value
        })
    }
    
    handleChangeTextCautat = (event) => {
        this.setState({
            textCautat: event.target.value
        })
    }
    
    // handleChangeLinkAccesat = (event) => {
    //     this.setState({
    //         linkAccesat: event.target.value
    //     })
    // }
     
    
    handleSearchClick = () => {
        let istoric = {
            idIstoric: x++,
            textCautat: this.state.textCautat,
         //   linkAccesat: this.state.linkAccesat
        }
        axios.post('https://serban-serbanadi.c9users.io/istoric', istoric).then((res) => {
            if(res.status === 200){
                this.props.istoricAdded(istoric)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
        
    render(){
        return(
            <div>
             <div>
                <h1>Cautare</h1>
                
                <input type="text" placeholder="text cautat" 
                    onChange={this.handleChangeTextCautat}
                    value={this.state.textCautat} />
              
                <button onClick={this.handleSearchClick}>Cauta</button>
            </div>
            </div>
            );
    }
}